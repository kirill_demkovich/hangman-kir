﻿using System;
using System.IO;

namespace HangmanKir
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"word.txt";
            Random random = new Random();

            string[] words = File.ReadAllLines("word.txt");
            
            while (true)
            {
                //выбор слова
                string word = words[random.Next(words.Length)];
                char[] viewword = new char[word.Length];

                Console.WriteLine($"we have a word with {word.Length} chars");
                for (int i = 0; i < word.Length; i++ )
                {
                    viewword[i] = '_';

                }
                int tryletter = 5;
                int openletter = 0;

                while (tryletter != 0 || openletter != word.Length - 1)
                {
                    Console.WriteLine("enter letter");
                    string inputletter = Console.ReadLine();
                    //проверка на количество введенных символов
                    if (inputletter.Length == 0 || inputletter.Length > 1 )
                    {
                        Console.WriteLine("enter only one letter!");
                        continue;
                    }
                    //проверка на введение только букв                    
                    if (!char.IsLetter(inputletter[0]))
                    {
                        Console.WriteLine("enter only chars!");
                        continue;
                    }

                    bool RightLetter = false;
                    for( int i = 0; i < word.Length; i++)
                    {
                        if (inputletter[0] == word[i] && viewword[i] == '_')
                        {
                            viewword[i] = inputletter[0];
                            RightLetter = true;
                        }
                    }

                    if (RightLetter)
                    {
                        Console.WriteLine("RIGHT LETTER, MAN!");
                    } else
                    {
                        Console.WriteLine("OY, MAN, you are fault!");
                        tryletter--;
                    }
                    Console.WriteLine(viewword);

                }
            }

        }
    }
}
